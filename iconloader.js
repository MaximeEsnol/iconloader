const IconLoader = function(icons){
      this.icons = icons;
      this.contents = [];
      this.iconsShortName =  [];
      

      this.load = async function(){
            for(let icon of icons){
                  let content = await fetch(icon);
                  let svg = await content.text();
                  this.contents.push(svg);
                  this.extract_name(icon);
            }
            if(this.icons.length === this.contents.length){
                  return true;
            } else {
                  return false;
            }
      };

      this.parse = async function(){
            if(await this.load()){

                  let iconElements = document.querySelectorAll('i.icon');

                  for(let icon of iconElements){
                        let iconIndex = this.iconsShortName.indexOf(icon.innerHTML + ".svg");
                        icon.innerHTML = this.contents[iconIndex];
                  }
            } else {
                  console.log("[IconLoader]: Unable to load (part of) the icons.");
            }
      };

      this.extract_name = function(path){
            let nameParts = path.split("/");
            let name = nameParts[nameParts.length - 1];

            this.iconsShortName.push(name);
      }
}